/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met: newbuttons
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/**@cond To Make Doxygen skip documentation generation for this file.
 * @{
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sdk_config.h"
#include "nrf.h"
#include "ble.h"
#include "ble_gatt.h"
#include "ble_hci.h"
#include "nordic_common.h"
#include "nrf_gpio.h"
#include "bsp_btn_ble.h"
#include "ble_advdata.h"
#include "ble_srv_common.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "app_error.h"
#include "nrf_pwr_mgmt.h"

#include "app_uart.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "string.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "math.h"

#define CONN_INTERVAL_DEFAULT           (uint16_t)(MSEC_TO_UNITS(7.5, UNIT_1_25_MS))    /**< Default connection interval used at connection establishment by central side. */

#define CONN_INTERVAL_MIN               (uint16_t)(MSEC_TO_UNITS(7.5, UNIT_1_25_MS))    /**< Minimum acceptable connection interval, in 1.25 ms units. */
#define CONN_INTERVAL_MAX               (uint16_t)(MSEC_TO_UNITS(500, UNIT_1_25_MS))    /**< Maximum acceptable connection interval, in 1.25 ms units. */
#define CONN_SUP_TIMEOUT                (uint16_t)(MSEC_TO_UNITS(8000,  UNIT_10_MS))    /**< Connection supervisory timeout (4 seconds). */

#define SLAVE_LATENCY                   0

#define ADVERTISING_LED                 BSP_BOARD_LED_0	/*! <LED telling if we're advertising or not. */
#define INDICATE_ALIVE_LED              BSP_BOARD_LED_1	/*! <LED telling if the device is even on. */

#define ON_PRESS_ADVERTISE                    BSP_BUTTON_0    /* Northwest button. */
#define ON_PRESS_ADVERTISE_EVENT              BSP_EVENT_KEY_0
#define DELAYED_ADVERTISE                     BSP_BUTTON_1    /* Northeast button. */
#define DELAYED_ADVERTISE_EVENT               BSP_EVENT_KEY_1

#define FAST_BLINK_INTERVAL		APP_TIMER_TICKS(200)
APP_TIMER_DEF(m_advertising_fast_blink_timer_id);                /*! <Timer used to toggle LED, indicting advertising on the dev. kit. */

#define SLOW_BLINK_INTERVAL		APP_TIMER_TICKS(750)
APP_TIMER_DEF(m_indicate_alive_led_slow_blink_timer_id);             /*! <Timer used to toggle LED for advertising mode indication on the dev.kit. */               

#define ADVERTISING_DELAYED_INTERVAL    APP_TIMER_TICKS(10000)   
APP_TIMER_DEF(m_advertising_delayed_timer_id);                   /*! <Timer used to wait after a button is pressed to start advertising.  */

#define APP_BLE_CONN_CFG_TAG    1                                               /**< A tag that refers to the BLE stack configuration. */
#define APP_BLE_OBSERVER_PRIO   3                                               /**< Application's BLE observer priority. You shouldn't need to modify this value. */

static uint8_t m_adv_handle  = BLE_GAP_ADV_SET_HANDLE_NOT_SET;                           /**< Advertising handle used to identify an advertising set. */
static uint8_t m_enc_advdata  [BLE_GAP_ADV_SET_DATA_SIZE_MAX];                            /**< Buffer for storing an encoded advertising set. */
static uint8_t m_enc_scandata [BLE_GAP_SCAN_BUFFER_EXTENDED_MIN];                            /**< Buffer for storing an encoded advertising set. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256      
#define UART_MAX_LEN  128

//type holding the two data stream toggle options./B
typedef enum
{
    UNCONTROLLED_DATA_STREAM = 0,
    CONTROLLED_DATA_STREAM
} data_stream_toggle_t;

typedef enum {
    SELECTION_0_dBm = 0,
    SELECTION_8_dBm = 8
} output_power_selection_t;

static output_power_selection_t   m_output_power_selected       = SELECTION_8_dBm;          /*! <Global variable holding the current output power selection.> */

static bool    m_app_initiated_disconnect   = false;          /*! <The application has initiated disconnect. Used to "tell" on_ble_gap_evt_disconnected() to not start advertising.> */
static bool    m_waiting_for_disconnect_evt = false;          /*! <Disconnect is initiated. The application has to wait for BLE_GAP_EVT_DISCONNECTED before proceeding.> */

#define adv_counter_min      1                        /*! <Minimum advertisement counter value. */
const uint8_t controlled_adv_max  = 100;              /*! <Max advertisements per button click during controlled mode. */
      uint8_t adv_counter         = adv_counter_min;  /*! <A counter for advertisings sent. */
      uint8_t adv_block_id        = 0;                /*! <A run-time unique ID per each block (10, 100, etc) of advertisements. */
bool  advertising                 = false;            /*! <Used to check if we're advertising. */ 

#define LAT_MAX           10 /*! <Maximum amount of characters in a latitude string. */
#define LONG_MAX          11 /*! <Maximum amount of characters in a longitude string. */
#define COORD_STRING_MAX  LAT_MAX + LONG_MAX /*! <Maximum amount of characters in the coord string. */
#define ENCODED_GPS_MAX         10  /*! <Maximum amount of encoded GPS characters from peripheral. */

char coord_string[COORD_STRING_MAX];  /*! <String to hold a useful GPS string.*/
uint8_t encoded_gps[ENCODED_GPS_MAX];    /*! <Holds the final encoded bits of GPS data.*/

void update_adv_data_set(uint8_t count);

static void set_current_adv_params_and_start_advertising(void);

/*! \brief Struct that contains pointers to the encoded advertising data. */
static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = NULL,
        .len    = 0

    }
};

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;    /*! < Handle of the current BLE connection .*/
static uint8_t m_gap_role     = BLE_GAP_ROLE_INVALID;       /*! < BLE role for this connection, see @ref BLE_GAP_ROLES */

// Scan parameters requested for scanning and connection.
static ble_gap_scan_params_t const m_scan_param =
{
    .active        = 0x00,
    .interval      = SCAN_INTERVAL,
    .window        = SCAN_WINDOW,
    .timeout       = 0x0000,
    .scan_phys     = BLE_GAP_PHY_1MBPS,
    .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
};

/*! \brief Connection parameters requested for connection. */

static ble_gap_conn_params_t m_conn_param =
{
    .min_conn_interval = CONN_INTERVAL_MIN,   //! <Minimum connection interval.
    .max_conn_interval = CONN_INTERVAL_MAX,   //! <Maximum connection interval.
    .slave_latency     = SLAVE_LATENCY,       //! <Slave latency.
    .conn_sup_timeout  = CONN_SUP_TIMEOUT     //! <Supervisory timeout.
};

/*! \brief fast blink timeout callback, inverts LED.
 */

static void adv_fast_blink_timeout_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context); 
  bsp_board_led_invert(ADVERTISING_LED);
}


/*! \brief slow blink timeout callback, inverts LED.
 */

static void led_adv_mode_slow_blink_timeout_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);    
  bsp_board_led_invert(INDICATE_ALIVE_LED);    
}

/*! \brief Function for handling BLE Stack events.
 *
 * Not implemented to be used in this project as there are no connections.
 */

static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{

  uint32_t              err_code;
  ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

  switch (p_ble_evt->header.evt_id)
  {

    case BLE_GATTS_EVT_SYS_ATTR_MISSING:
    {

      printf("BLE_GATTS_EVT_SYS_ATTR_MISSING\n");
      err_code = sd_ble_gatts_sys_attr_set(p_gap_evt->conn_handle, NULL, 0, 0);
      APP_ERROR_CHECK(err_code);

    } break;

    case BLE_GATTC_EVT_TIMEOUT: // Fallthrough.
    case BLE_GATTS_EVT_TIMEOUT:
    {
      printf("GATT timeout, disconnecting.\n");
      err_code = sd_ble_gap_disconnect(m_conn_handle,
                                      BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      APP_ERROR_CHECK(err_code);
    } break;

    default:
    {
      // Unimplemented BLE event.
    } break;

  }
}

/*! \brief Sets the advertising parameters.
 *
 * Needed to configure advertisements.
 * Configurations such as coded phy, connectable, extended, etc,
 * occur here.
 */

static void advertising_params_set(void)
{

  ret_code_t ret;
  ble_gap_adv_params_t adv_params =
  {
    .properties    =
    {
      .type = BLE_GAP_ADV_TYPE_EXTENDED_CONNECTABLE_NONSCANNABLE_UNDIRECTED,
    },
    .p_peer_addr   = NULL,
    .filter_policy = BLE_GAP_ADV_FP_ANY,
    .interval      = ADV_INTERVAL,
    .duration      = 0,

    .primary_phy   = BLE_GAP_PHY_CODED, // Long range.
    .secondary_phy = BLE_GAP_PHY_CODED, // -||-.
    .scan_req_notification = 1,
  };

  ret = sd_ble_gap_adv_set_configure(&m_adv_handle, NULL, &adv_params);
  APP_ERROR_CHECK(ret);
  
}

/*! \brief Function to start advertising. 
 *
 * Also sets the advertisement counter to 0, and increases adv_block_id which
 * indicates the 
*/

static void advertising_start(void)
{

  ret_code_t err_code;
  printf("Starting advertising.\n");

  err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, m_adv_handle, m_output_power_selected);
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
  APP_ERROR_CHECK(err_code);
  m_app_initiated_disconnect = false;

  err_code = app_timer_start(m_advertising_fast_blink_timer_id, FAST_BLINK_INTERVAL, NULL); 
  APP_ERROR_CHECK(err_code);

  advertising = true;
  adv_block_id++;
  adv_counter = adv_counter_min;
  
  // Updates the advertising data with counter.
  update_adv_data_set(adv_counter);

}

/*! \brief Callback function to start advertising after a timer has ended. */

static void on_advertise_delayed(void * p_context){

  UNUSED_PARAMETER(p_context);
  
  ret_code_t err_code = app_timer_stop(m_advertising_delayed_timer_id);
  APP_ERROR_CHECK(err_code);

  advertising_start();
}

/*! \brief   Stops the advertising, and stops LED 0 from blinking.
 */

static void stop_adv(void)
{

  uint32_t retval = sd_ble_gap_adv_stop(m_adv_handle);
  ret_code_t err_code;

  if (retval == NRF_SUCCESS) {
    advertising = false;
    
    bsp_board_led_off(ADVERTISING_LED);
    err_code = app_timer_stop(m_advertising_fast_blink_timer_id);
    APP_ERROR_CHECK(err_code);
    printf("Stopped advertising.\n");
  }

}

/*! \brief Function for the Timer initialization.
 *
 * Initializes the timer module. This creates application timers.
 */

static void timer_init(void)
{

  ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);

  // Creating the timers used to indicate the state/selection mode of the board.
  err_code = app_timer_create(&m_advertising_fast_blink_timer_id, APP_TIMER_MODE_REPEATED, adv_fast_blink_timeout_handler);
  APP_ERROR_CHECK(err_code);

  err_code = app_timer_create(&m_indicate_alive_led_slow_blink_timer_id, APP_TIMER_MODE_REPEATED, led_adv_mode_slow_blink_timeout_handler);
  APP_ERROR_CHECK(err_code);

  err_code = app_timer_start(&m_indicate_alive_led_slow_blink_timer_id, SLOW_BLINK_INTERVAL, NULL); 
  APP_ERROR_CHECK(err_code);

  err_code = app_timer_create(&m_advertising_delayed_timer_id,
                              APP_TIMER_MODE_SINGLE_SHOT, 
                              on_advertise_delayed);
  APP_ERROR_CHECK(err_code);


}

/*! \brief Function for initializing the BLE stack.
 *
 * Initializes the SoftDevice and the BLE event interrupt.
 */

static void ble_stack_init(void)
{

  ret_code_t err_code;
  err_code = nrf_sdh_enable_request();
  APP_ERROR_CHECK(err_code);

  // Configure the BLE stack using the default settings.
  // Fetch the start address of the application RAM.
  uint32_t ram_start = 0;
  err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
  APP_ERROR_CHECK(err_code);

  // Enable BLE stack.
  err_code = nrf_sdh_ble_enable(&ram_start);
  APP_ERROR_CHECK(err_code);

  // Register a handler for BLE events.
  NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);

}


/*! \brief Function for initializing GAP parameters.
 *
 * This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 * device including the device name and the preferred connection parameters.
 * However, in this project we will not be connected.
 */

static void gap_params_init(void)
{

  ret_code_t              err_code;
  ble_gap_conn_sec_mode_t sec_mode;

  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  err_code = sd_ble_gap_device_name_set(&sec_mode,
                                        (uint8_t const *)DEVICE_NAME,
                                        strlen(DEVICE_NAME));
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_ppcp_set(&m_conn_param);
  APP_ERROR_CHECK(err_code);

}

/*! \brief Function for initializing power management.
 */

static void power_management_init(void)
{

  ret_code_t ret;
  ret = nrf_pwr_mgmt_init();
  APP_ERROR_CHECK(ret);

}


/*! \brief Function for handling the idle state (main loop).
 *
 * Handle any pending operation(s), then sleep until the next event occurs.
 */

static void idle_state_handle(void)
{
    nrf_pwr_mgmt_run();
}

bool is_ascii_num_code(char c) {
    
    if ((c >= 48) && (c <= 57)) {
        return true;
    }
    
    return false;
}

/*! \brief Compresses GPS coordinates to take up fewer bytes in advertisements.
 *
 * ASCII representation of two digits take up two bytes.
 * Coupling heir 
 *
 * For instance, a '5' and a '9' have ASCII values 53 and 57, respectively.
 * Together they add up to two bytes to store as ASCII representations.
 * However, combining their <i>represented values</i> to 59 will take up only one byte.
 *
 * For coupled digits in order such that a zero digit is first, like "05",
 * they are represented as a value between 200 and 209. 200 representing "00",
 * 201 representing "01", etc.
 *
 * A coordinate string (with decimal points and cardinal direction omitted; we know how many
 *  characters are in lat and long.) may look like:
 *
 * "5925059980175519098". Nineteen bytes.
 *
 * This becomes a series of byte-sized values:
 * 
 * 59 25 205 99 80 17 55 19 209 8. Ten bytes.
 *
 */

static void encode_latitude_longitude(uint8_t* coord_string_raw, uint8_t* encoded_gps_arr) {

  uint8_t forward;
  uint8_t encoded_i = 0;
  uint8_t len = COORD_STRING_MAX;

  for(uint8_t i = 0; i < len; i++) {
  
    if (is_ascii_num_code(coord_string_raw[i])) {
        
      if ((i + 1) != len) {
          
        if (coord_string_raw[i] == '0') {
            encoded_gps_arr[encoded_i] = (coord_string_raw[i + 1] - 48) + 200;
        }
        else {
          encoded_gps_arr[encoded_i] = ((coord_string_raw[i] - 48) * 10) + (coord_string_raw[i + 1] - 48);
        }
        
        i++;
      }
      else {
        encoded_gps_arr[encoded_i] = coord_string_raw[i] - 48;
      }
      
      encoded_i++;
    }
  }

  while (encoded_i < ENCODED_GPS_MAX){
    encoded_gps_arr[encoded_i] = '\0';
    encoded_i++;
  }
}

/*! \brief Updates the advertising data with counter and GPS coordinates.
 *
 * This function updates the advertising data by setting the adv_data buffer
 * to a null pointer. While it is set to a null pointer a new buffer is created
 * and new, updated advertising data is set.
 * 
 * New advertising data is 10 bytes' worth of GPS coordinates, as well as 
 * a count (to show which packet in the "block" we are sending), and a block id, to
 * indicate which block of advertisements that is being sent. For example, a 
 * hundred advertisements may be sent at a time. The first hundred will have ID
 * 0, the second 1, third 2, and so forth.
 * 
 * \p count Advertisement counter within this data set.
 * #adv_block_id Advertisement block we're on.
 */

void update_adv_data_set(uint8_t count)
{
  /*
   * Changing the data while advertising requires setting a different data buffer
   * before setting a new (or modified) data buffer.
   * A solution is to just use NULL instead of a different data buffer.
   */
  ret_code_t ret = sd_ble_gap_adv_set_configure(&m_adv_handle, NULL, NULL);

  if (ret == NRF_SUCCESS){
    ble_advdata_manuf_data_t  manuf_data; // Variable to hold manufacturer specific data
    
    /*
     * The data, being the latitude and longitude of the latest valid position.
     * Then the per-advertisement-counter (one-indexed outwardly).
     * Then the adv_block_id, to help divide data sets of controlled adv data.
     * 
     */
    
    uint8_t data[] = {
                      encoded_gps[0],
                      encoded_gps[1],
                      encoded_gps[2],
                      encoded_gps[3],
                      encoded_gps[4],
                      encoded_gps[5],
                      encoded_gps[6],
                      encoded_gps[7],
                      encoded_gps[8],
                      encoded_gps[9],
                      count,
                      adv_block_id,
    };

    manuf_data.company_identifier             =  0x0059; // Nordics company ID
    manuf_data.data.p_data                    = data;
    manuf_data.data.size                      = sizeof(data);

    ble_advdata_t const adv_data =
    {
      .name_type          = BLE_ADVDATA_FULL_NAME,
      .flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE,
      .p_manuf_specific_data = &manuf_data,
      .include_appearance = false,
    };

    ret = ble_advdata_encode(&adv_data, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
    APP_ERROR_CHECK(ret);

    /* "When this function is used to update advertising data while advertising,
     * this parameter must be NULL." - ble_gap.h, line 1776.
     * The paramater in question is the third parameter.
     */
    ret = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, NULL);
  }
}

/*! \brief Radio notification interrupt request handler
 *
 * Logic that follows AFTER an advertisement, because this is the only
 * way we know if we have advertised.
 */

void RADIO_NOTIFICATION_IRQHandler()
{

  if (advertising) {
    
    #ifdef ADV_RADIO_NOTIF_DEBUG
    printf("Sent adv packet nr: %d\n", adv_counter);
    #endif


    // See if the next advertisement would surpass the maximum amount of advertisements.
    if (controlled_adv_max < (adv_counter + 1)) {
      stop_adv();
    }
    else {
      adv_counter++;
      update_adv_data_set(adv_counter);
    }

  }

}

/*! \brief Initiates radio notification, setting interrupts.
 *
 * Must be called after ble_stack_init() and before advertising starts.
 */
static void radio_notification_init()
{
  uint32_t  err_code;

  err_code = sd_nvic_ClearPendingIRQ(RADIO_NOTIFICATION_IRQn);
  APP_ERROR_CHECK(err_code);

  err_code = sd_nvic_EnableIRQ(RADIO_NOTIFICATION_IRQn);
  APP_ERROR_CHECK(err_code);

  err_code = sd_radio_notification_cfg_set(NRF_RADIO_NOTIFICATION_TYPE_INT_ON_INACTIVE, 
                                           NRF_RADIO_NOTIFICATION_DISTANCE_800US);
  APP_ERROR_CHECK(err_code);
}


/*! \brief Function for handling events from the button handler module.
 */
void bsp_evt_handler(bsp_event_t event)
{

  ret_code_t err_code;	 

  // Set the correct parameters, depending on the button pushed.		
  switch (event)
  {

    case ON_PRESS_ADVERTISE_EVENT:
    {
      if (!advertising){
        advertising_start();
        printf("Starting controlled send of advertisements.");
      }
    }break;

    case DELAYED_ADVERTISE_EVENT:
    {
      if (!advertising){

        err_code = app_timer_create(&m_advertising_delayed_timer_id,
                                    APP_TIMER_MODE_SINGLE_SHOT, 
                                    on_advertise_delayed);

        APP_ERROR_CHECK(err_code);
        err_code = app_timer_start(m_advertising_delayed_timer_id,
                                   ADVERTISING_DELAYED_INTERVAL, NULL);
        APP_ERROR_CHECK(err_code);
      }
    }break;
    default:
      break;
  }

}

/*! \brief Function for initializing buttons and leds.
 */
static void buttons_leds_init(void)
{

    ret_code_t err_code;
   
    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_evt_handler);
    APP_ERROR_CHECK(err_code);

}

/*! \brief Function for starting advertising with the current
 *         selections of output power, phy, advertising.
 */
static void set_current_adv_params_and_start_advertising(void)
{
  
  // Set the parameters for advertising.
  advertising_params_set();
  // Set the data for advertising.
  update_adv_data_set(0);

}

/*! \brief Extracts relevant geographical data from gps_array and stores it in global variables.
 *
 *  Example format of gps_array:
 *
 *  $GPGLL,5925.06393,N,01755.19166,E,131954.00,A,A*69
 *
 *  What we want from this is:
 *         5925063930175519166
 *                         Extract into coord_string.
 *                         return true.
 *
 *  We do not add the cardinal directions (N/S, W/E)
 *  because this software is to be used in the northeastern
 *  quadrant of the GPS map reference, and this data is known.
 *  At the end of this software, it was necessary to save each and every
 *  byte possible. Could make a 2x2 grid and have 4 byte codes for NE NW SE SW,
 *  taking up just one byte as a code in the advertisement.
 *
 *  Example of gps_array containing non-data from the GPS:
 *
 *  $GNGLL,,,,,120908.00,V,N*56
 *  
 *  In this case, Returns false if N or S is not found as per the standard GLL format.
 *  This indicates that the gps data is empty like this: "$GPGLL,,,,," etc.
 *  (Could also be indicated by non-length of latitude pointer, or length of gps_array.)
 */
static bool extract_latitude_longitude(uint8_t* gps_array, uint8_t gps_array_max) {
  
  uint8_t coord_string_i = 0;
  uint8_t lat_i = 0;
  uint8_t long_i = 0;
  uint8_t time_i = 0;
  uint8_t len;
  char*   p;
  char*   next;

  // Position at beginning, spelling out $GNGLL or $GPGLL
  p = strtok(gps_array, ",");
  
  // Longitude start
  p = strtok(NULL, ",");

  len = strlen(p);
  
  next = strtok(NULL, ","); // N or S

  if (*next != 'N' && *next != 'S') {
    return false;
  } 
  
  // Values of longitude.
  while((*p != '\0') && (lat_i < len)) {
    coord_string[coord_string_i] = *p;
    ++lat_i;
    ++p;
    ++coord_string_i;
  }

  p = strtok(NULL, ",");  // Latitude start

  len = strlen(p); // Values of latitude

  while ((*p != '\0') && (long_i < len)) {
    coord_string[coord_string_i] = *p;
    ++long_i;
    ++p;
    ++coord_string_i;
  }

  next = strtok(NULL, ","); // W or E

  for (int i = COORD_STRING_MAX; i > coord_string_i; i--) {
    coord_string[i] = '\0';
  }

  return true;
}

/*! \brief Returns true if string argument is in GLL format.
 *  
 *  Any string argument beginning with "...GLL" or "...GLL" will return true.
 *  This to find strings starting with "$GPGLL" or "GNGLL".
 */

bool match_GLL(uint8_t* gps_array)
{
  
  char* matcher = "GLL";

  uint8_t GLL_str_start = 3;
  uint8_t GLL_str_end = 5;
  bool match = true;
  
  for(uint8_t i = GLL_str_start; i <= GLL_str_end; i++) {
    if (gps_array[i] != matcher[i - GLL_str_start]) {
      match = false;
    }
  }
  
  return match;

}

/*! \brief Handling the data received over UART per character.*/

void uart_event_handle(app_uart_evt_t * p_event)
{
  static uint8_t data_array[UART_MAX_LEN];
  static uint8_t new_byte;
  static uint8_t index;

  switch (p_event->evt_type)
  {
    case APP_UART_COMMUNICATION_ERROR:
      // unimplemented com error. Could print something here for debug purposes.
      break;

    case APP_UART_DATA:
    UNUSED_VARIABLE(app_uart_get(&new_byte));

    if (new_byte == '\n') {
      data_array[index] = new_byte;
      index++;
      data_array[index] = '\0';
      
      #ifdef DEBUG_UART_DATA_ARRAY
        printf("%s", data_array); // Would typically be a whole GPS sentence. $GNGLL,lat,etc.
      #endif

      /* If it's the GPGLL or GNGLL Data,
      * then we will extract the longitude and longitude. */
      if(match_GLL(data_array)) {

        if (extract_latitude_longitude(data_array, index)) {
          encode_latitude_longitude(coord_string, encoded_gps);
        }

      }

        index = 0;
    } 
    else if (new_byte == '$') {
      // Start of GPS line, e.g. "$GNGLL...".
      index = 0;
      data_array[index] = new_byte;
      index++;
    }
    else {
      data_array[index] = new_byte;
      index++;
    }

    break;
    default:
    break;
  }

}

/*! \brief  Function for initializing the UART module. */

static void uart_init(void)
{

  uint32_t                     err_code;
  app_uart_comm_params_t const comm_params =
  {
  .rx_pin_no    = RX_PIN_NUMBER,
  .tx_pin_no    = TX_PIN_NUMBER,
  .rts_pin_no   = RTS_PIN_NUMBER,
  .cts_pin_no   = CTS_PIN_NUMBER,
  .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
  .use_parity   = false,
#if defined (UART_PRESENT)
  .baud_rate    = NRF_UART_BAUDRATE_9600
#else
  .baud_rate    = NRF_UARTE_BAUDRATE_9600
#endif
  };
  

  APP_UART_FIFO_INIT(&comm_params,
                     UART_RX_BUF_SIZE,
                     UART_TX_BUF_SIZE,
                     uart_event_handle,
                     APP_IRQ_PRIORITY_LOWEST,
                     err_code);
  APP_ERROR_CHECK(err_code);
}

/*! \brief Manually startup the external 32KHz 
 */

static void external_clock_init(){
  NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
  NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos);
  NRF_CLOCK->TASKS_LFCLKSTART = 1;
  while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0)
  {
  }
  NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
}

int main(void)
{
  // Initialize.

  // Initialize external 32KHz clock prior to enabling softdevice.
  external_clock_init();

  uart_init();
  timer_init();
  buttons_leds_init();
  power_management_init();
  ble_stack_init();
  gap_params_init();
  radio_notification_init();
  set_current_adv_params_and_start_advertising();

  // Enter main loop.
  for (;;)
  {
    idle_state_handle();
  }
}